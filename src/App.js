import React, { Component } from 'react';
import './App.css';
import WizardComponent from "./wizard-component";
import axios from "axios";

class App extends Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    step: 1
  }
  updateState = (e) => {
    this.setState({
      [e.target.dataset.type]: e.target.value
    });
  }
  validateFields = (step) => {
    let strRegEx = /^[a-z]+$/i;
    let emailRegEx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const { firstName, lastName, email, phone } = this.state;
    if (step === 1) {
      if (!firstName || !strRegEx.test(firstName)) {
        alert("Please valid First name.");
        return false;
      }
      if (!lastName || !strRegEx.test(lastName)) {
        alert("Please valid Last name.");
        return false;
      }
    } else if (step === 2) {
      if (!email || !emailRegEx.test(email)) {
        alert("Please enter a valid email.");
        return false;
      }
      if (phone.length !== 10) {
        alert("Please enter valid phone number of length 10");
        return false;
      }
    }
    return true;
  }
  step1() {
    const { firstName, lastName } = this.state;
    return (
      <div>
        <div>First Name: <input required type="text" data-type="firstName" onChange={this.updateState} value={firstName} /></div>
        <div>Last Name: <input required type="text" data-type="lastName" onChange={this.updateState} value={lastName} /></div>
      </div>
    )
  }
  step2() {
    const { email, phone } = this.state;
    return (
      <div>
        <div>Email: <input type="email" data-type="email" onChange={this.updateState} value={email} /></div>
        <div>Phone no: <input type="number" data-type="phone" onChange={this.updateState} value={phone} /></div>
      </div>
    )
  }
  onNext = (e) => {
    preventDefault(e);

    if (this.validateFields(this.state.step)) {
      this.setState(({ step }) => ({
        step: ++step
      }));
    }
  }

  onPrev = (e) => {
    preventDefault(e);
    this.setState(({ step }) => ({
      step: --step
    }));
  }

  onFinish = (e) => {
    preventDefault(e);

    const { firstName, lastName, email, phone } = this.state;
    if (this.validateFields(this.state.step)) {
      axios.post("fake/rest/post", {
        firstName, lastName, email, phone
      });
    }

  }

  onCancel = (e) => {
    preventDefault(e);
    alert("cancel");
    window.location.reload();
  }

  render() {
    const { step } = this.state;
    let stepNames = ["Personal Info", "Contact Info"];
    return (
      <div className="App">
        <WizardComponent
          step={step}
          stepNames={stepNames}
        >
          <form onSubmit={preventDefault}>
            {this["step" + step]()}
            <Footer
              stepNames={stepNames}
              step={step}
              onNext={this.onNext}
              onPrev={this.onPrev}
              onFinish={this.onFinish}
              onCancel={this.onCancel}
            />
          </form>
        </WizardComponent>
      </div>
    );
  }
}

export default App;


const Footer = ({ onNext, onPrev, onFinish, onCancel, step, stepNames }) => {
  const nextButton = <button type="submit" onClick={onNext}>Next</button>
  const prevButton = <button type="none" onClick={onPrev}>Previous</button>
  const finishButton = <button type="submit" onClick={onFinish}>Finish</button>
  const cancelButton = <button type="none" className="cancel-button" onClick={onCancel}>Cancel</button>

  return (
    <div className="wizard-footer">
      {step > 1 && prevButton}
      {step < stepNames.length && nextButton}
      {step === stepNames.length && finishButton}

      {/* cancel is placed in the left because the user should not accidentally click on it*/}
      {cancelButton}
    </div>
  )
}

const preventDefault = (e) => {
  e.preventDefault()
}