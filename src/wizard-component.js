import React, { Component } from "react";
import classname from "classname";
import PropTypes from "prop-types";
import "./wizard-component.css";


export default class WidgetComponent extends Component {
    static defaultProps = {
        stepNames: [],
        step: 1
    }

    static propTypes = {
        stepNames: PropTypes.array.isRequired,
        step: PropTypes.number.isRequired
    }

    render() {
        let children = React.Children.only(this.props.children);
        if (children.type !== "form") {
            throw new Error("Accepts only form element");
        }
        const { stepNames, step } = this.props;

        return (
            <div className="wizard-component">
                <Progress
                    stepNames={stepNames}
                    step={step}
                />
                <div className="wizard-body">
                    {children}
                </div>
            </div>
        )
    }
}

const Progress = ({ stepNames, step }) => {
    const length = stepNames.length;
    const width = 100 / length;

    return (
        <div className="wizard-component-progress">
            {
                stepNames.map((stepName, i) => {
                    return (
                        <div
                            key={i}
                            style={{
                                width: width + "%"
                            }}
                            className={classname({
                                active: step === i + 1,
                                completed: step > i + 1
                            })}
                        >
                            <div
                                className="progress-bar-circle"
                            >
                                {i + 1}
                            </div>
                            <div className="wizard-step-name">{stepName}</div>
                        </div>
                    )
                })
            }
        </div>
    )
}
