import React from 'react';
import ReactDOM from 'react-dom';
import WizardComponent from './wizard-component';

it('WizardComponent accepts only 1 child', () => {
  const div = document.createElement('div');
  ReactDOM.render(<WizardComponent>
    <form>
    </form>
  </WizardComponent>, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('WizardComponent does not accept non wizard component', () => {
  expect(() => {
    const div = document.createElement('div');
    ReactDOM.render(<WizardComponent>
      <div>
      </div>
    </WizardComponent>, div);
    ReactDOM.unmountComponentAtNode(div);
  }).toThrowError();
});
